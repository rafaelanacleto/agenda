<?php

class Agenda_model extends CI_Model {
    
    private $id;
    private $horarios_id;
    private $usuarios_id;
    private $dt_agenda;
    private $descricao;
    private $porcentagem;

    public function __construct()
    {
        parent::__construct();    
    }

    public function getAgenda($id_login = false){

        if($id_login){
            $this->db->select('A.*');
            $this->db->from('AGENDA A');
            $this->db->where('A.USUARIOS_ID',$id_login);            
            $query=$this->db->get();
            if($query->num_rows() > 0){
                    return $query->result();
            }
        }
        return false;
    }



        
}