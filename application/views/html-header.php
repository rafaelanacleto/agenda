<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistema de Agenda</title>

	<?php
		echo link_tag('assets/bootstrap/css/bootstrap.css') .                            
			link_tag('assets/bootstrap/css/bootstrap-responsive.css') .
            link_tag('assets/bootstrap/css/bootstrap-theme.css').
			link_tag('assets/bootstrap/css/estilos.css');
	?> 
		
    <script src="<?php echo base_url('assets/js/jquery-2.1.4.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.mask.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.js'); ?>"></script>
	
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
