<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 private $dados = false;

	public function index()
	{

		$this->load->model('Agenda_model');
		$this->dados['agenda'] = $this->Agenda_model->getAgenda(1);

		$this->load->view('html-header');
		$this->load->view('header');
		$this->load->view('agenda', $this->dados);
		$this->load->view('footer');
		$this->load->view('html-footer');		
	}

	public function login()
	{
		$this->load->view('html-header');
		$this->load->view('login');
		$this->load->view('html-footer');		

	}
}
